import sys; sys.setrecursionlimit(15000)
import time

import cairo
import pycha.line

if __name__ == "__main__":

    dataSet = (
        ("NaiveApproach", [(100, 0.098002910614), 
                          (1000, 0.151422023773),
                          (10000, 1.13025999069),
                          (100000, 10.8543918133),
                          (1000000, 96.6269128323)]),
        ("SegmentTreeApproach", 
                        [(100, 0.100892066956),
                        (1000, 0.160159111023),
                        (10000, 0.240512132645),
                        (100000, 0.915969133377),
                        (1000000, 9.38035011292)])
    )
    filename = "compare.png"

    surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, 1024, 720)


    options = {
        'axis': {
            'x': {
                'interval': 50000,
                'label': 'array size', 
                },
            'y': {
                # 'interval': 5,
                'label': 'time'
                },
            },
        'legend': {
            # 'hide': True,
            },
        'title': "NaiveApproach Vs SegmentTreeApproach",
        'background': {
            'baseColor': '#f0f0f0',
            },
        'shouldFill': False,
    }
    chart = pycha.line.LineChart(surface, options)

    chart.addDataset(dataSet)
    chart.render()

    surface.write_to_png(filename)
