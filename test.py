import sys; sys.setrecursionlimit(15000)
from collections import defaultdict, OrderedDict, deque
import time
import random

from model import NaiveApproach, SegmentTreeApproach


class TestEngine(object):
    def __init__(self, solver_kls):
        self.solver_kls = solver_kls

    def test(self, array, pq):
        solver = self.solver_kls(array)
        while len(pq):
            op, data = pq.popleft()
            if op == "modify":
                solver.modify(data[0], data[1])
            else:
                r = solver.query(data[0], data[1])
                if r != data[2]:
                    raise Exception("result: %s, expect:%s"%(r, data[2]))


    @classmethod
    def parse_testcase(cls, filename="testdata/test.in"):
        with open(filename) as f:
            array = map(int, f.readline().split())
            pq = deque()
            for line in f:
                items = line.split()
                pq.append([items[0], tuple(map(int, items[1:]))])
        return array, pq

    @classmethod
    def gen_testcases(cls, filename="testdata/test.in", size=100, op_size=100):
        array = [random.randint(0, 100) for i in range(size)]
        solver = NaiveApproach(array)
        d = defaultdict(int)
        op_list = ["modify", "query"]
        f = open(filename, "w")
        f.write(" ".join(map(str, array)) + "\n")
        for i in range(op_size):
            op = random.choice(op_list)
            if op == "modify":
                index = random.randint(0, size-1)
                val = random.randint(0, 100)
                solver.modify(index, val)
                f.write("%s %s %s\n"%(op, index, val))
            else:
                start = random.randint(0, size-1)
                end = random.randint(start, size-1)
                r = solver.query(start, end)
                f.write("%s %s %s %s\n"%(op, start, end, r))
            d[op] += 1
        f.close()


if __name__ == "__main__":
    # 
    test_engine = TestEngine(NaiveApproach)
    test_engine = TestEngine(SegmentTreeApproach)
    # test_engine.test(array, d)
    for size in map(lambda x:10**x, range(2, 7)):
        start_time = time.time()
        array, d = TestEngine.parse_testcase(filename="testdata/test%s.in"%size)
        test_engine.test(array, d)
        # TestEngine.gen_testcases(filename="testdata/test%s.in"%size, size=size, op_size=5000)
        print size, time.time() - start_time








