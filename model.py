



class SegmentNode(object):
    def __init__(self, start, end, total):
        self.start, self.end, self.total = start, end, total
        self.left, self.right = None, None



class SegmentTree(object):
    def __init__(self, array=None):
        array = array or []
        def helper(start, end):
            if start == end:
                return SegmentNode(start, end, array[start])
            
            root = SegmentNode(start, end, 0)
            mid = (start + end) / 2
            root.left = helper(start, mid)
            root.right = helper(mid+1, end)
            root.total = root.left.total + root.right.total
            return root
        self.root = helper(0, len(array)-1)


    def query(self, start, end):
        def helper(root, start, end):
            if start == root.start and end == root.end:
                return root.total
            
            mid = (root.start + root.end) / 2
            
            if mid < start:
                return helper(root.right, start, end)
            if mid+1 > end:
                return helper(root.left, start, end)
            
            return helper(root.left, start, mid) + \
                        helper(root.right, mid+1, end)
        return helper(self.root, start, end)

    def modify(self, index, val):
        def helper(root, index, value):
            if root.start == root.end:
                root.total = value
                return
        
            if root.left.end >= index:
                helper(root.left, index, value)
            else:
                helper(root.right, index, value)
            
            root.total = root.left.total + root.right.total
        helper(self.root, index, val)



class ArrayLikeMixin(object):
    def query(self, start, end):
        pass

    def modify(self, index, val):
        pass

class NaiveApproach(ArrayLikeMixin):
    def __init__(self, array):
        self.array = array or []

    def query(self, start, end):
        total = 0
        for i in range(start, end + 1):
            total += self.array[i]
        return total

    def modify(self, index, val):
        self.array[index] = val



class SegmentTreeApproach(SegmentTree):
    pass


if __name__ == "__main__":
    sgtree = SegmentTree([1, 2])
    print sgtree.query(0, 1)




